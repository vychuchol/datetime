// Implements date and time parser/formatter as subset of ISO 8601.
// See details: https://www.w3.org/TR/NOTE-datetime
// Supported date-time format: 2017-10-21T12:34:56Z
// Minimum unit is second.
// Overleap second is ignored.
// Duration supports only hours, minutes and seconds.
// Duration format is: PT12H34M56S

package datetime

import (
	"fmt"
	"regexp"
	"strconv"
)

const (
	dateBaseRegexp = "([0-9]{4})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])"
	dateRegexp     = "^" + dateBaseRegexp + "$"
	dateFormat     = "%04d-%02d-%02d"
	timeBaseRegexp = "([01][0-9]|2[0-3]):([0-5][0-9])(?::([0-5][0-9])(?:\\.[0-9]+)?)?"
	timeRegexp     = "^" + timeBaseRegexp + "$"
	timeFormat     = "%02d:%02d:%02dZ"
	dateTimeRegexp = "^" + dateBaseRegexp + "T" + timeBaseRegexp + "(?:Z|[+-](?:[01][0-9]|2[0-3]):(?:[0-5][0-9]))$"
	dateTimeFormat = dateFormat + "T" + timeFormat
	durationRegexp = "^PT([0-9]+H)?([0-9]+M)?([0-9]+S)?$"
)

var (
	date     = regexp.MustCompile(dateRegexp)
	time     = regexp.MustCompile(timeRegexp)
	dateTime = regexp.MustCompile(dateTimeRegexp)
)

// Date represents date with year, month and day.
type Date struct {
	Year  int
	Month int
	Day   int
}

// Time represents time with hour, minute and second.
type Time struct {
	Hour   int
	Minute int
	Second int
}

// DateTime represents date and time with year, month, day, hour, minute and second.
type DateTime struct {
	Date
	Time
}

// Duration represents time in seconds.
type Duration int

func ParseDate(s string) (*Date, bool) {
	year, month, day, ok := parseDate(s)
	if !ok || !validDate(year, month, day) {
		return nil, false
	}
	return &Date{
		Year:  year,
		Month: month,
		Day:   day,
	}, true
}

func parseDate(s string) (year int, month int, day int, ok bool) {
	parts := date.FindStringSubmatch(s)
	if len(parts) == 0 {
		return 0, 0, 0, false
	}
	year, _ = strconv.Atoi(parts[1])
	month, _ = strconv.Atoi(parts[2])
	day, _ = strconv.Atoi(parts[3])
	ok = true
	return
}

func validDate(year int, month int, day int) bool {
	if year < 0 || year > 9999 {
		return false
	}
	if month < 1 || month > 12 {
		return false
	}
	days := []int{31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
	if day < 1 || day > days[month-1] {
		return false
	}
	// leap year
	if month == 2 {
		// TODO optimize
		leap := true
		if year%4 != 0 {
			leap = false
		} else if year%100 != 0 {
		} else if year%400 != 0 {
			leap = false
		}
		if !leap && day == 29 {
			return false
		}
	}
	return true
}

func (d *Date) Parse(s string) bool {
	year, month, day, ok := parseDate(s)
	if !ok || !validDate(year, month, day) {
		return false
	}
	d.Year = year
	d.Month = month
	d.Day = day
	return true
}

func (d *Date) Valid() bool {
	return validDate(d.Year, int(d.Month), d.Day)
}

func (d *Date) String() string {
	return fmt.Sprintf(dateFormat, d.Year, d.Month, d.Day)
}

func ParseTime(s string) (*Time, bool) {
	hour, minute, second, ok := parseTime(s)
	if !ok {
		return nil, false
	}
	return &Time{
		Hour:   hour,
		Minute: minute,
		Second: second,
	}, true
}

func parseTime(s string) (hour int, minute int, second int, ok bool) {
	parts := time.FindStringSubmatch(s)
	if len(parts) == 0 {
		return 0, 0, 0, false
	}
	hour, _ = strconv.Atoi(parts[1])
	minute, _ = strconv.Atoi(parts[2])
	if parts[3] != "" {
		second, _ = strconv.Atoi(parts[3])
	} else {
		second = 0
	}
	ok = true
	return
}

func (t *Time) Parse(s string) bool {
	hour, minute, second, ok := parseTime(s)
	if !ok {
		return false
	}
	t.Hour = hour
	t.Minute = minute
	t.Second = second
	return true
}

func (t *Time) Valid() bool {
	if t.Hour < 0 || t.Hour > 23 {
		return false
	}
	if t.Minute < 0 || t.Minute > 59 {
		return false
	}
	if t.Second < 0 || t.Second > 59 {
		return false
	}
	return true
}

func (t *Time) String() string {
	return fmt.Sprintf(timeFormat, t.Hour, t.Minute, t.Second)
}

func ParseDateTime(s string) (*DateTime, bool) {
	year, month, day, hour, minute, second, ok := parseDateTime(s)
	if !ok {
		return nil, false
	}
	if !validDate(year, month, day) {
		return nil, false
	}
	return &DateTime{
		Date{
			Year:  year,
			Month: month,
			Day:   day,
		},
		Time{
			Hour:   hour,
			Minute: minute,
			Second: second,
		},
	}, true
}

func parseDateTime(s string) (year int, month int, day int, hour int, minute int, second int, ok bool) {
	parts := dateTime.FindStringSubmatch(s)
	if len(parts) == 0 {
		return 0, 0, 0, 0, 0, 0, false
	}
	year, _ = strconv.Atoi(parts[1])
	month, _ = strconv.Atoi(parts[2])
	day, _ = strconv.Atoi(parts[3])
	hour, _ = strconv.Atoi(parts[4])
	minute, _ = strconv.Atoi(parts[5])
	if parts[6] != "" {
		second, _ = strconv.Atoi(parts[6])
	} else {
		second = 0
	}
	ok = true
	return
}

func (dt *DateTime) Parse(s string) bool {
	year, month, day, hour, minute, second, ok := parseDateTime(s)
	if !ok {
		return false
	}
	dt.Year = year
	dt.Month = month
	dt.Day = day
	dt.Hour = hour
	dt.Minute = minute
	dt.Second = second
	return true
}

func (dt *DateTime) Valid() bool {
	return dt.Date.Valid() && dt.Time.Valid()
}

func (dt *DateTime) String() string {
	return fmt.Sprintf(dateTimeFormat, dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second)
}

func ParseDuration(s string) (Duration, bool) {
	if ok, _ := regexp.MatchString(durationRegexp, s); !ok {
		return 0, false
	}
	s = s[2:] // remove PT
	if s == "" {
		return 0, false
	}
	d := 0
	last := 0
	for i, c := range s {
		switch c {
		case 'H':
			hours, _ := strconv.Atoi(s[last:i])
			last = i
			d += hours * 60 * 60
		case 'M':
			minutes, _ := strconv.Atoi(s[last:i])
			last = i
			d += minutes * 60
		case 'S':
			seconds, _ := strconv.Atoi(s[last:i])
			last = i
			d += seconds
		}
	}
	return Duration(d), true
}

func (d Duration) Valid() bool {
	return d >= 0
}

func (d Duration) Hours() int {
	if d < 0 {
		return 0
	}
	return int(d) / 3600
}

func (d Duration) Minutes() int {
	if d < 0 {
		return 0
	}
	return int(d) / 60
}

func (d Duration) Seconds() int {
	if d < 0 {
		return 0
	}
	return int(d)
}

// String returns formatted duration.
// The largest unit is hour, zero values are ignored.
func (d Duration) String() string {
	seconds := int(d)
	if seconds < 0 {
		return "PT0S"
	}
	if seconds < 60 {
		return fmt.Sprintf("PT%02dS", seconds)
	}
	minutes := seconds / 60
	seconds = seconds % 60
	if minutes < 60 {
		return fmt.Sprintf("PT%02dM%02dS", minutes, seconds)
	}
	hours := minutes / 60
	minutes = minutes % 60
	return fmt.Sprintf("PT%02dH%02dM%02dS", hours, minutes, seconds)
}
