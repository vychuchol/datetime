package datetime

import "testing"

func TestParseDate(t *testing.T) {
	_, _, _, ok := parseDate("")
	if ok {
		t.Errorf("\"\" -> ok=%v", ok)
	}

	y, m, d, ok := parseDate("2017-02-01")
	if y != 2017 || m != 2 || d != 1 || !ok {
		t.Errorf("\"2017-02-01\" -> y=%d, m=%d, d=%d, ok=%v", y, m, d, ok)
	}
}
